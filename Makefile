all: build

PREFIX?=gcr.io/google_containers
FLAGS=
ARCH?=amd64
ALL_ARCHITECTURES=amd64 arm arm64 ppc64le s390x
ML_PLATFORMS=linux/amd64,linux/arm,linux/arm64,linux/ppc64le,linux/s390x
GOLANG_VERSION?=1.8

ifndef TEMP_DIR
TEMP_DIR:=$(shell mktemp -d /tmp/statster.XXXXXX)
endif

VERSION?=v0.1
GIT_COMMIT:=$(shell git rev-parse --short HEAD)

TESTUSER=
ifdef REPO_DIR
DOCKER_IN_DOCKER=1
TESTUSER=jenkins
else
REPO_DIR:=$(shell pwd)
endif

# You can set this variable for testing and the built image will also be tagged with this name
OVERRIDE_IMAGE_NAME?=

# If this session isn't interactive, then we don't want to allocate a
# TTY, which would fail, but if it is interactive, we do want to attach
# so that the user can send e.g. ^C through.
INTERACTIVE := $(shell [ -t 0 ] && echo 1 || echo 0)
TTY=
ifeq ($(INTERACTIVE), 1)
	TTY=-t
endif

SUPPORTED_KUBE_VERSIONS=1.6.0
TEST_NAMESPACE=heapster-e2e-tests

HEAPSTER_LDFLAGS=-w -X github.com/adfin/statster/version.HeapsterVersion=$(VERSION) -X github.com/adfin/statster/version.GitCommit=$(GIT_COMMIT)

fmt:
	find . -type f -name "*.go" | grep -v "./vendor*" | xargs gofmt -s -w

build: clean fmt
	GOARCH=$(ARCH) CGO_ENABLED=0 go build -ldflags "$(HEAPSTER_LDFLAGS)" -o statster github.com/adfin/statster/metrics

sanitize:
	hooks/check_boilerplate.sh
	hooks/check_gofmt.sh
	hooks/run_vet.sh

test-unit: clean sanitize build
ifeq ($(ARCH),amd64)
	GOARCH=$(ARCH) go test --test.short -race ./... $(FLAGS)
else
	GOARCH=$(ARCH) go test --test.short ./... $(FLAGS)
endif

test-unit-cov: clean sanitize build
	hooks/coverage.sh

test-integration: clean build
	go test -v --timeout=60m ./integration/... --vmodule=*=2 $(FLAGS) --namespace=$(TEST_NAMESPACE) --kube_versions=$(SUPPORTED_KUBE_VERSIONS) --test_user=$(TESTUSER) --logtostderr

influxdb:
	ARCH=$(ARCH) PREFIX=$(PREFIX) make -C influxdb build

grafana:
	ARCH=$(ARCH) PREFIX=$(PREFIX) make -C grafana build

push-influxdb:
	PREFIX=$(PREFIX) make -C influxdb push

push-grafana:
	PREFIX=$(PREFIX) make -C grafana push

# TODO(luxas): As soon as it's working to push fat manifests to gcr.io, reenable this code
#./manifest-tool:
#	curl -sSL https://github.com/luxas/manifest-tool/releases/download/v0.3.0/manifest-tool > manifest-tool
#	chmod +x manifest-tool

clean:
	rm -f statster

.PHONY: all build sanitize test-unit test-unit-cov test-integration container grafana influxdb clean
