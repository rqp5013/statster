FROM gliderlabs/alpine
RUN apk --update add --no-cache ca-certificates

ADD statster /bin
ENTRYPOINT /bin/statster
