## Metrics

Statster organizes its metrics into the following hierachy.


| Cluster Metric Name | Description |
|------------|-------------|
| cluster/pods/.* | POD statiatics (definition) |
| cluster/ig/.* | Instance group (definition)
| cluster/cpu/.* | CPU stats (defition) |
| cluster/mem/.* | Memory stats (defition) |
| cluster/net/.* | Network stats (defition) |

| Namespace Metric Name | Description |
|------------|-------------|
| namespace/(NS) | Root of per namespace statistics |
| namespace/(NS)/pods/.* | POD statiatics |
| namespace/(NS)/cpu/.* | CPU stats (defition) |
| namespace/(NS)/mem/.* | Memory stats (defition) |
| namespace/(NS)/net/.* | Network stats (defition) |
| namespace/(NS)/workloads/ | Root of per namespace, workload statistics |
| namespace/(NS)/workloads/daemonset/(NAME)/(CR_STATS) | Compute resource stats |
| namespace/(NS)/workloads/deployment/(NAME)/(CR_STATS) | Compute resource stats |
| namespace/(NS)/workloads/jobs/(NAME)/(CR_STATS) | Compute resource stats |
| namespace/(NS)/workloads/statefulset/(NAME)/(CR_STATS) | Compute resource stats |
| namespace/(NS)/services/{NAME}/(CR_STATS)

 
| Node Metric Name | Description |
|------------|-------------|
| nodes/(IG)/(NODE)/(CR_STATS) | Per node compute resource stats |

### Compute resource group

| Metric Name | Description |
|------------|-------------|
| ./ | Root prefix |
| ./pod/(POD) | Aggregate pod statistics |
| ./cpu/(CPU) | |
| ./mem/(MEM) | |
| ./net/(NET)| |

### POD Stats

| Metric Name | Description |
|------------|-------------|
| ./count | count |
| ./running | count |
| ./terminated | count |

### CPU Stats

| Metric Name | Description |
|------------|-------------|
| cpu/limit | CPU hard limit in millicores. |
| cpu/node_capacity | Cpu capacity of a node. |
| cpu/node_allocatable | Cpu allocatable of a node. |
| cpu/node_reservation | Share of cpu that is reserved on the node allocatable. |
| cpu/node_utilization | CPU utilization as a share of node allocatable. |
| cpu/request | CPU request (the guaranteed amount of resources) in millicores. |
| cpu/usage | Cumulative CPU usage on all cores. |
| cpu/usage_rate | CPU usage on all cores in millicores. |

### Memory Stats

| Metric Name | Description |
|------------|-------------|
| memory/limit | Memory hard limit in bytes. |
| memory/major_page_faults | Number of major page faults. |
| memory/major_page_faults_rate | Number of major page faults per second. |
| memory/node_capacity | Memory capacity of a node. |
| memory/node_allocatable | Memory allocatable of a node. |
| memory/node_reservation | Share of memory that is reserved on the node allocatable. |
| memory/node_utilization | Memory utilization as a share of memory allocatable. |
| memory/page_faults | Number of page faults. |
| memory/page_faults_rate | Number of page faults per second. |
| memory/request | Memory request (the guaranteed amount of resources) in bytes. |
| memory/usage | Total memory usage. |
| memory/cache | Cache memory usage. |
| memory/rss | RSS memory usage. |
| memory/working_set | Total working set usage. Working set is the memory being used and not easily dropped by the kernel. |

### Network Stats

| Metric Name | Description |
|------------|-------------|
| network/rx | Cumulative number of bytes received over the network. |
| network/rx_errors | Cumulative number of errors while receiving over the network. |
| network/rx_errors_rate | Number of errors while receiving over the network per second. |
| network/rx_rate | Number of bytes received over the network per second. |
| network/tx | Cumulative number of bytes sent over the network |
| network/tx_errors | Cumulative number of errors while sending over the network |
| network/tx_errors_rate | Number of errors while sending over the network |
| network/tx_rate | Number of bytes sent over the network per second. |

### Filesystem stats

| Metric Name | Description |
|------------|-------------|
| filesystem/usage | Total number of bytes consumed on a filesystem. |
| filesystem/limit | The total size of filesystem in bytes. |
| filesystem/available | The number of available bytes remaining in a the filesystem |
| filesystem/inodes | The number of available inodes in a the filesystem |
| filesystem/inodes_free | The number of free inodes remaining in a the filesystem |

