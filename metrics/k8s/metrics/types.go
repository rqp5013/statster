// Copyright 2017 Adfin Solutions Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package metrics

import (
	"github.com/adfin/statster/metrics/core"
)

func GetResourceType(this *core.MetricSet) string {
	return this.Labels[core.LabelMetricSetType.Key]
}

type K8sMetrics struct {
	*core.MetricSet
}

func (this *K8sMetrics) Namespace() (v string, found bool) {
	v, found = this.MetricSet.Labels[core.LabelNamespaceName.Key]
	return
}

func (this *K8sMetrics) InstancegroupName() (v string, found bool) {
	v, found = this.MetricSet.Labels[core.LabelInstanceGroup.Key]
	return
}

func (this *K8sMetrics) NodeName() (v string, found bool) {
	v, found = this.MetricSet.Labels[core.LabelNodename.Key]
	return
}

func (this *K8sMetrics) PodName() (v string, found bool) {
	v, found = this.MetricSet.Labels[core.LabelPodName.Key]
	return
}

// Get the named key for this MetricsSet (based on correct type)
func (this *K8sMetrics) Key() (string, bool) {
	switch GetResourceType(this.MetricSet) {
	case core.MetricSetTypeNodeInstanceGroup:
		ig, f := this.InstancegroupName()
		return core.InstancegroupKey(ig), f
	case core.MetricSetTypeNode:
		ig, f1 := this.InstancegroupName()
		node, f2 := this.NodeName()

		missing := !f1 || !f2
		return core.NodeKey(ig, node), !missing
	case core.MetricSetTypePod:
		ns, f1 := this.Namespace()
		pod, f2 := this.PodName()

		missing := !f1 || !f2
		return core.PodKey(ns, pod), !missing
	}

	return "", false
}
