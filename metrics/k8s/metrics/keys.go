package metrics

import (
	"github.com/adfin/statster/metrics/core"
)

func NodeKey(metrics K8sMetrics) (string, bool) {
	ig, found := metrics.InstancegroupName()
	if !found {
		return "", found
	}

	node, found := metrics.NodeName()
	if !found {
		return "", found
	}

	return core.NodeKey(ig, node), true
}

func InstancegroupKey(metrics K8sMetrics) (string, bool) {
	ig, found := metrics.InstancegroupName()
	if !found {
		return "", found
	}

	return core.InstancegroupKey(ig), true
}
