// Copyright 2017 Adfin Solutions Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package metrics

import (
	"github.com/adfin/statster/metrics/core"
)

type IterateTypeFunc func(key string, metricSet K8sMetrics) error

func IterateType(metricSets map[string]*core.MetricSet, resourceType string, fun IterateTypeFunc) error {
	for key, metricSet := range metricSets {
		rType := GetResourceType(metricSet)

		if rType == resourceType {
			wrap := K8sMetrics{metricSet}
			if err := fun(key, wrap); err != nil {
				return err
			}
		}
	}

	return nil
}
