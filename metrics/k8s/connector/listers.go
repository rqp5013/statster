package connector

import (
	"time"

	"github.com/pkg/errors"

	"k8s.io/apimachinery/pkg/fields"
	k8_client "k8s.io/client-go/kubernetes"
	k8_listers "k8s.io/client-go/listers/core/v1"
	k8_listers_ext "k8s.io/client-go/listers/extensions/v1beta1"
	k8_api "k8s.io/client-go/pkg/api/v1"
	ki_api_ext "k8s.io/client-go/pkg/apis/extensions/v1beta1"
	"k8s.io/client-go/tools/cache"
)

type Listers struct {
	ServiceLister    k8_listers.ServiceLister
	PodLister        k8_listers.PodLister
	NodeLister       k8_listers.NodeLister
	ReplicaSetLister k8_listers_ext.ReplicaSetLister
}

func GetListers(client *k8_client.Clientset) (out Listers, err error) {
	out.PodLister, _, err = getPodLister(client)
	if err != nil {
		return out, errors.Wrap(err, "Failed to create PodLister")
	}

	out.NodeLister, _, err = getNodeLister(client)
	if err != nil {
		return out, errors.Wrap(err, "Failed to create NodeLister")
	}

	out.ServiceLister, _, err = getServiceListers(client)
	if err != nil {
		return out, errors.Wrap(err, "Failed to create ServiceLister")
	}

	out.ReplicaSetLister, _, err = getReplicasetListers(client)
	if err != nil {
		return out, errors.Wrap(err, "Failed to create ReplicaSetLister")
	}

	return out, nil
}

func getPodLister(kubeClient *k8_client.Clientset) (k8_listers.PodLister, *cache.Reflector, error) {
	lw := cache.NewListWatchFromClient(kubeClient.Core().RESTClient(), "pods", k8_api.NamespaceAll, fields.Everything())
	store := cache.NewIndexer(cache.MetaNamespaceKeyFunc, cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc})
	podLister := k8_listers.NewPodLister(store)
	reflector := cache.NewReflector(lw, &k8_api.Pod{}, store, time.Hour)
	reflector.Run()
	return podLister, reflector, nil
}

func getNodeLister(kubeClient *k8_client.Clientset) (k8_listers.NodeLister, *cache.Reflector, error) {
	lw := cache.NewListWatchFromClient(kubeClient.Core().RESTClient(), "nodes", k8_api.NamespaceAll, fields.Everything())
	store := cache.NewIndexer(cache.MetaNamespaceKeyFunc, cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc})
	nodeLister := k8_listers.NewNodeLister(store)
	reflector := cache.NewReflector(lw, &k8_api.Node{}, store, time.Hour)
	reflector.Run()

	return nodeLister, reflector, nil
}

func getServiceListers(kubeClient *k8_client.Clientset) (k8_listers.ServiceLister, *cache.Reflector, error) {
	lw := cache.NewListWatchFromClient(kubeClient.Core().RESTClient(), "services", k8_api.NamespaceAll, fields.Everything())
	store := cache.NewIndexer(cache.MetaNamespaceKeyFunc, cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc})
	serviceLister := k8_listers.NewServiceLister(store)
	reflector := cache.NewReflector(lw, &k8_api.Service{}, store, time.Hour)
	reflector.Run()

	return serviceLister, reflector, nil
}

func getReplicasetListers(kubeClient *k8_client.Clientset) (k8_listers_ext.ReplicaSetLister, *cache.Reflector, error) {
	client := kubeClient.ExtensionsV1beta1().RESTClient()

	lw := cache.NewListWatchFromClient(client, "replicasets", k8_api.NamespaceAll, fields.Everything())
	store := cache.NewIndexer(cache.MetaNamespaceKeyFunc, cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc})
	replicaSetLister := k8_listers_ext.NewReplicaSetLister(store)
	reflector := cache.NewReflector(lw, &ki_api_ext.ReplicaSet{}, store, time.Hour)
	reflector.Run()

	return replicaSetLister, reflector, nil
}
