package processors

import (
	"github.com/adfin/statster/metrics/core"
)

var podMetrics = []string{
	core.MetricCpuLimit.Name,
	core.MetricCpuRequest.Name,
	core.MetricCpuUsage.Name,
	core.MetricMemoryCache.Name,
	core.MetricMemoryLimit.Name,
	core.MetricMemoryMajorPageFaults.Name,
	core.MetricMemoryPageFaults.Name,
	core.MetricMemoryRSS.Name,
	core.MetricMemoryRequest.Name,
	core.MetricMemoryUsage.Name,
	core.MetricMemoryWorkingSet.Name,
	core.MetricNetworkRx.Name,
	core.MetricNetworkRxErrors.Name,
	core.MetricNetworkRxErrorsRate.Name,
	core.MetricNetworkRxRate.Name,
	core.MetricNetworkTx.Name,
	core.MetricNetworkTxErrors.Name,
	core.MetricNetworkTxErrorsRate.Name,
	core.MetricNetworkTxRate.Name,
	core.MetricPodsCount.Name,
	core.MetricPodsFailed.Name,
	core.MetricPodsPending.Name,
	core.MetricPodsRunning.Name,
	core.MetricPodsSucceded.Name,
	core.MetricPodsUnknown.Name,
}

var nodeMetrics = []string{
	core.MetricNodeCpuAllocatable.Name,
	core.MetricNodeCpuCapacity.Name,
	core.MetricNodeMemoryAllocatable.Name,
	core.MetricNodeMemoryCapacity.Name,
}
