// Copyright 2017 Adfin Solutions Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package processors

import (
	"github.com/golang/glog"

	k8m "github.com/adfin/statster/metrics/k8s/metrics"

	"github.com/adfin/statster/metrics/core"
	"github.com/adfin/statster/metrics/k8s/connector"
	"github.com/pkg/errors"
	k8_meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8_api "k8s.io/client-go/pkg/api/v1"
)

type WorkloadAggregator struct {
	connector.Listers
}

func (this *WorkloadAggregator) Name() string {
	return "workload_aggregator"
}

func (this *WorkloadAggregator) Process(batch *core.DataBatch) (*core.DataBatch, error) {
	workloads := make(map[string]*core.MetricSet)

	each := func(key string, ms k8m.K8sMetrics) error {
		pod, err := this.getPod(ms.MetricSet)
		if err != nil {
			errors.Wrap(err, "getPod failed")
		}

		workloadMs, _ := this.getWorkload(workloads, &pod.ObjectMeta)
		aggregate(ms.MetricSet, workloadMs, podMetrics)

		return nil
	}

	if err := k8m.IterateType(batch.MetricSets, core.MetricSetTypePod, each); err != nil {
		return nil, err
	}

	merge(workloads, batch)
	return batch, nil
}

func (this *WorkloadAggregator) getPod(ms *core.MetricSet) (*k8_api.Pod, error) {
	namespace := ms.Labels[core.LabelNamespaceName.Key]
	podName := ms.Labels[core.LabelPodName.Key]
	return this.PodLister.Pods(namespace).Get(podName)
}

func (this *WorkloadAggregator) workloadInfo(objMeta *k8_meta.ObjectMeta) (string, string, bool) {
	refs := objMeta.OwnerReferences

	switch len(refs) {
	case 1:
	case 0: // Orphan
		return "unkown", "__ALL__", false
	default:
		glog.Warningf("Multiple OwnerReference not supported, count: %i", len(refs))
		return "unkown", "__ALL__", false
	}

	switch refs[0].Kind {
	case "DaemonSet":
		return "daemonsets", refs[0].Name, false
	case "StatefulSet":
		return "statefulsets", refs[0].Name, false
	case "ReplicaSet":
		rs, err := this.ReplicaSetLister.ReplicaSets(objMeta.Namespace).Get(refs[0].Name)
		if err != nil {
			glog.Errorf("getReplicaSets failed: %s", err.Error())
			break
		}

		return this.workloadInfo(&rs.ObjectMeta)
	case "Deployment":
		return "deployments", refs[0].Name, false
	default:
		glog.Warningf("Uknown OnwerReference kind: %s", refs[0].Kind)
	}

	return "unkown", "__ALL__", false
}

func (this *WorkloadAggregator) getWorkload(metricsSet map[string]*core.MetricSet, objMeta *k8_meta.ObjectMeta) (*core.MetricSet, string) {
	workload, name, _ := this.workloadInfo(objMeta)
	key := core.Workload(objMeta.Name, workload, name)
	ms, found := metricsSet[key]
	if found {
		return ms, key
	}

	out := &core.MetricSet{
		MetricValues: make(map[string]core.MetricValue),
		Labels: map[string]string{
			core.LabelMetricSetType.Key: core.MetricSetTypeWorkload,
			core.LabelNamespaceName.Key: objMeta.Namespace,
			core.LabelWorkloadKind.Key:  workload,
			core.LabelWorkloadName.Key:  name,
		},
	}

	metricsSet[key] = out
	return out, key
}
