// Copyright 2017 Adfin Solutions Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package processors

import (
	"github.com/adfin/statster/metrics/core"
	k8m "github.com/adfin/statster/metrics/k8s/metrics"
	"github.com/golang/glog"
)

var instanceGroupMetrics = append(podMetrics, nodeMetrics...)

// Does not add any nodes.
type NodeInstancegroupAggregator struct {
}

func (this *NodeInstancegroupAggregator) Name() string {
	return "node_instancegroup_aggregator"
}

func (this *NodeInstancegroupAggregator) Process(batch *core.DataBatch) (*core.DataBatch, error) {
	node_count := uint64(1)

	each := func(key string, ms k8m.K8sMetrics) error {
		igKey, found := k8m.InstancegroupKey(ms)
		if !found {
			key, _ := ms.Key()
			glog.V(8).Infof("Skipping pod %s: no node/ig info", key)
			return nil
		}

		ig, found := batch.MetricSets[igKey]
		if !found {
			ig = this.newInstancegroup(ms.MetricSet)
			batch.MetricSets[igKey] = ig
		}

		// Just inject node count
		core.AppendIntMetric(ig, &core.MetricInstanceGroupNodeCount, &node_count)

		if err := aggregate(ms.MetricSet, ig, instanceGroupMetrics); err != nil {
			return err
		}

		return nil
	}

	return batch, k8m.IterateType(batch.MetricSets, core.MetricSetTypeNode, each)
}

func (this *NodeInstancegroupAggregator) newInstancegroup(node *core.MetricSet) *core.MetricSet {
	return &core.MetricSet{
		MetricValues: make(map[string]core.MetricValue),
		Labels: map[string]string{
			core.LabelMetricSetType.Key: core.MetricSetTypeNodeInstanceGroup,
			core.LabelInstanceGroup.Key: node.Labels[core.LabelInstanceGroup.Key],
		},
	}
}
