// Copyright 2015 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package processors

import (
	"github.com/adfin/statster/metrics/core"
	k8m "github.com/adfin/statster/metrics/k8s/metrics"
	"github.com/golang/glog"
)

var nodeMetricsFromPod = []string{
	core.MetricCpuRequest.Name,
	core.MetricCpuLimit.Name,
	core.MetricMemoryRequest.Name,
	core.MetricMemoryLimit.Name,
	core.MetricPodsCount.Name,
	core.MetricPodsUnknown.Name,
	core.MetricPodsPending.Name,
	core.MetricPodsRunning.Name,
	core.MetricPodsSucceded.Name,
	core.MetricPodsFailed.Name,
}

// Does not add any nodes.
type NodeAggregator struct {
}

func (this *NodeAggregator) Name() string {
	return "node_aggregator"
}

func (this *NodeAggregator) Process(batch *core.DataBatch) (*core.DataBatch, error) {
	each := func(key string, ms k8m.K8sMetrics) error {
		nodeKey, found := k8m.NodeKey(ms)
		if !found {
			key, _ := ms.Key()
			glog.V(8).Infof("Skipping pod %s: no node/ig info", key)
			return nil
		}

		node, found := batch.MetricSets[nodeKey]
		if !found {
			glog.Errorf("No node found %s: %v", nodeKey, ms.Labels)
			return nil
		}

		if err := aggregate(ms.MetricSet, node, nodeMetricsFromPod); err != nil {
			return err
		}

		return nil
	}

	return batch, k8m.IterateType(batch.MetricSets, core.MetricSetTypePod, each)
}
