package processors

import (
	"github.com/golang/glog"

	"github.com/adfin/statster/metrics/core"
	"github.com/adfin/statster/metrics/k8s/connector"
	kube_labels "k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/pkg/api/v1"
)

type ServiceAggregator struct {
	connector.Listers
}

func (this *ServiceAggregator) Name() string {
	return "workload_aggregator"
}

func (this *ServiceAggregator) Process(batch *core.DataBatch) (*core.DataBatch, error) {
	out := make(map[string]*core.MetricSet)

	services, err := this.ServiceLister.List(kube_labels.Everything())
	if err != nil {
		return nil, err
	}

	for _, service := range services {
		serviceMs, serviceKey := this.getService(out, service)
		if !validService(service) {
			glog.V(8).Infof("Service not qualified, not targetting pods: %s", serviceKey)
			continue
		}

		selector := kube_labels.SelectorFromSet(service.Spec.Selector)

		pods, err := this.PodLister.Pods(service.Namespace).List(selector)
		if err != nil {
			return nil, err
		}

		for _, podSpec := range pods {
			podKey := core.PodKey(podSpec.Namespace, podSpec.Name)
			podMs, found := batch.MetricSets[podKey]
			if found {
				aggregate(podMs, serviceMs, podMetrics)
			} else {
				glog.V(8).Infof("Not found POD: %s for service: %s", podKey, serviceKey)
				// TODO: log
			}
		}

		out[serviceKey] = serviceMs
	}

	merge(out, batch)
	return batch, nil
}

func (this *ServiceAggregator) getService(metricsSet map[string]*core.MetricSet, service *v1.Service) (*core.MetricSet, string) {
	key := core.ServiceKey(service.Namespace, service.Name)
	ms, found := metricsSet[key]
	if found {
		return ms, key
	}

	out := &core.MetricSet{
		MetricValues: make(map[string]core.MetricValue),
		Labels: map[string]string{
			core.LabelMetricSetType.Key: core.MetricSetTypeService,
			core.LabelNamespaceName.Key: service.Namespace,
			core.LabelServiceName.Key:   service.Name,
		},
	}

	return out, key
}

func validService(service *v1.Service) bool {
	switch {
	case service.Spec.Type == v1.ServiceTypeExternalName:
		return false
	case len(service.Spec.ExternalName) > 0:
		return false
	case len(service.Spec.Selector) == 0:
		return false
	default:
		return true
	}
}
